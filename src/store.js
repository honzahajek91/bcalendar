import {createStore} from 'vuex'
import axios from "axios";

const store = createStore({
    state() {
        return {
            reservations: []
        }
    },
    mutations: {
        SET_API_DATA(state, payload) {
            state.reservations = payload.reservations;
            state.configLoaded = true;
        },
    },
    actions: {
        loadApiData(context) {
            axios
                .get('data.json')
                .then((res) => {
                    context.commit('SET_API_DATA', res.data);
                })
                .catch((error) => {
                    console.log(error);
                }).finally(() => {
                // this.setApiData();
                // this.loading = false;
                // this.dataLoaded = true;
            });
        }
    }
});

export default store;